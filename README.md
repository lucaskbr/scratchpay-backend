# ❤️ Scratchpay backend

## 💡 Assumptions for Filtering Clinics by Multiple Criteria
When developing an app to filter clinics by multiple criteria, it is important to consider the functionality of searching by state name or state code. This is because state information is a common search criteria for medical clinics.

It is essential to treat the state name and state code as separate search criteria. Combining them in a single field and performing a search by matching characters can lead to incorrect search results.

For example, consider the following list of states and their corresponding codes:

- stateName: 'Alaska' stateCode: AK
- stateName: 'Alabama' stateCode: AL

Instead of merging the state name and state code into a single field, it is best to keep them as separate search criteria.

## 🚀 Development Process
Here are the steps taken in developing the app to filter clinics by multiple criteria:

- Started creating the test cases
- Created the types and schemas that would be used for the API endpoints and validations
- Created mapping functions from the API to the backend
- Added a HTTP framework to handle the requests
- Created fetch calls to the API endpoints
- Installed useful plugins
- Created the Swagger docs
- Glued everything together
- Created the pipeline to deploy the app
- Fixed some bugs

By following these steps, the app was successfully developed to filter clinics by multiple criteria, with the added functionality of searching by state name or state code.

## 🏃️ Running Locally

```
cp .env.example .env
docker build -t scratchpay-backend . --no-cache
docker run --publish 8080:8080 scratchpay-backend
```

## 📖  Access the live docs

```
https://scratchpay-backend.fly.dev/docs
```

## 🔎 Search by Multiple Criteria Live Endpoint

```
https://scratchpay-backend.fly.dev/clinic-providers?stateCode=FL&availabilityFrom=15&availabilityTo=20&name=Good

or

curl --request GET \
  --url 'https://scratchpay-backend.fly.dev/clinic-providers?stateCode=FL&availabilityFrom=15&availabilityTo=20&name=Good' \
  --header 'Content-Type: application/json' \
  --data '{}'
```
