# Stage 1: Build the app
FROM node:18.15-alpine AS build

# Create a directory for the app code
WORKDIR /app

# Copy package.json and package-lock.json into the container
COPY package*.json ./

COPY *.env ./

# Install app dependencies
RUN npm install

# Copy the rest of the app code into the container
COPY . .

# Build the app
RUN npm run build

RUN ls /app

# Stage 2: Run the app
FROM node:18.15-alpine

# Create a directory for the app code
WORKDIR /app

# Copy only the built app and its dependencies from the previous stage
COPY --from=build /app/dist ./dist
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/package*.json ./
COPY --from=build /app/*.env ./

RUN ls /app

# Expose the port the app listens on
EXPOSE 8080

# Start the app
CMD ["npm", "start"]
