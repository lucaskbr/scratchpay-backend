import { TSchema } from '@sinclair/typebox'
import { TypeCompiler } from '@sinclair/typebox/compiler'

export const validateDataBasedOnSchema = (
  schema: TSchema,
  data: unknown,
): boolean => {
  const C = TypeCompiler.Compile(schema)

  return !!C.Check(data)
}
