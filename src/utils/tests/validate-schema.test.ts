import { Type } from '@sinclair/typebox'
import { describe, expect, it } from 'vitest'
import { randSuperheroName, randFood, randUuid } from '@ngneat/falso'

import { validateDataBasedOnSchema } from '../validate-schema.js'

describe('validateDataBasedOnSchema', () => {
  const dogSchema = Type.Object({
    id: Type.String(),
    name: Type.String(),
    favoriteFood: Type.String(),
  })

  it('should return true when the schema matchs the data', () => {
    const dog = {
      id: randUuid(),
      name: randSuperheroName(),
      favoriteFood: randFood(),
    }

    const dogIsValid = validateDataBasedOnSchema(dogSchema, dog)

    expect(dogIsValid).toBe(true)
  })

  describe('should return false when the data doesnt match the schema', () => {
    it('when field is missing', () => {
      const dog = {
        id: randUuid(),
        favoriteFood: randFood(),
      }

      const dogIsValid = validateDataBasedOnSchema(dogSchema, dog)

      expect(dogIsValid).toBe(false)
    })

    it('when field is with wrong type', () => {
      const dog = {
        id: randUuid(),
        name: 1,
        favoriteFood: randFood(),
      }

      const dogIsValid = validateDataBasedOnSchema(dogSchema, dog)

      expect(dogIsValid).toBe(false)
    })
  })
})
