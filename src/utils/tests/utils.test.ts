import { it, describe, expect } from 'vitest'
import { compareHours } from '../utils.js'

describe('utils', () => {
  describe('compareHours', () => {
    it('should return true if the first hour is greater than the second hour', () => {
      expect(compareHours('12:30', 'greaterThen', '10:00')).toBe(true)
      expect(compareHours('12:00', 'greaterThen', '11:59')).toBe(true)
      expect(compareHours('06:30', 'greaterThen', '06:00')).toBe(true)
    })

    it('should return true if the first hour is greater than or equal to the second hour', () => {
      expect(compareHours('12:00', 'greaterThenOrEqual', '12:00')).toBe(true)
      expect(compareHours('12:30', 'greaterThenOrEqual', '11:59')).toBe(true)
      expect(compareHours('06:30', 'greaterThenOrEqual', '06:00')).toBe(true)
    })

    it('should return true if the first hour is less than the second hour', () => {
      expect(compareHours('02:00', 'lessThen', '10:00')).toBe(true)
      expect(compareHours('12:00', 'lessThen', '12:01')).toBe(true)
      expect(compareHours('06:00', 'lessThen', '06:30')).toBe(true)
    })

    it('should return true if the first hour is less than or equal to the second hour', () => {
      expect(compareHours('12:00', 'lessThenOrEqual', '12:00')).toBe(true)
      expect(compareHours('02:00', 'lessThenOrEqual', '10:00')).toBe(true)
      expect(compareHours('06:00', 'lessThenOrEqual', '06:30')).toBe(true)
    })

    it('should return false if an invalid operator is passed in', () => {
      expect(compareHours('01:00', 'invalidOperator' as any, '12:00')).toBe(
        false,
      )
    })
  })
})
