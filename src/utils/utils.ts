export const compareHours = (
  firstHour: string,
  operator:
    | 'greaterThen'
    | 'greaterThenOrEqual'
    | 'lessThen'
    | 'lessThenOrEqual',
  secondHour: string,
): boolean => {
  // Parse the hours and minutes from the first and second hour strings
  const [firstHourInMinutes, firstMinuteInMinutes] = firstHour
    .split(':')
    .map(Number)
  const [secondHourInMinutes, secondMinuteInMinutes] = secondHour
    .split(':')
    .map(Number)

  // Calculate the total number of minutes for the first and second hour
  const totalMinutesFirst =
    firstHourInMinutes * 60 + (firstMinuteInMinutes || 0)
  const totalMinutesSecond =
    secondHourInMinutes * 60 + (secondMinuteInMinutes || 0)

  // Compare the two hour values based on the given operator
  switch (operator) {
    case 'greaterThen':
      return totalMinutesFirst > totalMinutesSecond
    case 'greaterThenOrEqual':
      return totalMinutesFirst >= totalMinutesSecond
    case 'lessThen':
      return totalMinutesFirst < totalMinutesSecond
    case 'lessThenOrEqual':
      return totalMinutesFirst <= totalMinutesSecond
    default:
      // Return false if the operator is invalid
      return false
  }
}
