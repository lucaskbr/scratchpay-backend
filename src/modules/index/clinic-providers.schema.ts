import { Static, Type } from '@sinclair/typebox'

export const coolMessage = Type.Object({
  message: Type.String(),
})

export type CoolMessage = Static<typeof coolMessage>
