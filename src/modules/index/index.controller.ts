import { FastifyReply, FastifyRequest } from 'fastify'
import { Controller, GET } from 'fastify-decorators'
import { coolMessage } from './clinic-providers.schema.js'

@Controller({ route: '/' })
export default class ClinicProvidersController {
  @GET({
    url: '',
    options: {
      schema: {
        operationId: 'index',
        description:
          'Root route of the app, it will just return 200 with a cool message',
        tags: ['clinic-providers'],
        response: {
          200: coolMessage,
        },
      },
    },
  })
  async list(
    request: FastifyRequest,
    reply: FastifyReply,
  ): Promise<FastifyReply> {
    return reply.send({ message: 'Scratchpay rocks!' })
  }
}
