import { Static, Type } from '@sinclair/typebox'
import { clinicProvider } from '../../schemas/clinic-provider.js'

export const availability = Type.Object({
  from: Type.String(),
  to: Type.String(),
})

export const clinicProvidersListQueryString = Type.Object({
  name: Type.Optional(Type.String()),
  stateCode: Type.Optional(Type.String()),
  stateName: Type.Optional(Type.String()),
  availabilityFrom: Type.Optional(Type.String()),
  availabilityTo: Type.Optional(Type.String()),
})

export type ClinicProvidersListQueryString = Static<
  typeof clinicProvidersListQueryString
>

export const clinicProvidersListResponse = Type.Array(clinicProvider)
