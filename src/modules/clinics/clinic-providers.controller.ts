import { FastifyReply, FastifyRequest } from 'fastify'
import { Controller, GET } from 'fastify-decorators'
import {
  listDentalClinics,
  listVetClinics,
} from '../../apis/clinic-providers.api.js'
import {
  ClinicProvidersListQueryString,
  clinicProvidersListQueryString,
  clinicProvidersListResponse,
} from './clinic-providers.schema.js'
import {
  filterClinicsByMultipleCriteria,
  mapUnionClinicListToClinicProvidersList,
} from './clinic-providers.service.js'

@Controller({ route: '/clinic-providers' })
export default class ClinicProvidersController {
  @GET({
    url: '/',
    options: {
      schema: {
        operationId: 'listClinicProviders',
        description:
          'fetch vet clinics and dent clinics from a third party api parse the result and combine the all the data into a single response',
        tags: ['clinic-providers'],
        querystring: clinicProvidersListQueryString,
        response: {
          200: clinicProvidersListResponse,
        },
      },
    },
  })
  async list(
    request: FastifyRequest<{
      Querystring: ClinicProvidersListQueryString
    }>,
    reply: FastifyReply,
  ): Promise<FastifyReply> {
    const criteria = request.query

    const [vetClinicProviderList, dentalClinicProviderList] = await Promise.all(
      [listVetClinics(), listDentalClinics()],
    )
    const unionClinicList = [
      ...vetClinicProviderList,
      ...dentalClinicProviderList,
    ]

    const clinicProviders =
      mapUnionClinicListToClinicProvidersList(unionClinicList)

    const filteredClinics = filterClinicsByMultipleCriteria(
      clinicProviders,
      criteria,
    )

    return reply.send(filteredClinics)
  }
}
