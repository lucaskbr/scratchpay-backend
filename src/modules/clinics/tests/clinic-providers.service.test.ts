import { randState, randStateAbbr, randSuperheroName } from '@ngneat/falso'
import { describe, expect, it } from 'vitest'
import { ClinicProvider } from '../../../schemas/clinic-provider.js'
import { DentalClinicProvider } from '../../../schemas/dental-clinic-provider.js'
import { VetClinicProvider } from '../../../schemas/vet-clinic-provider.js'
import {
  Criteria,
  filterClinicsByMultipleCriteria,
  mapDentalClinicToClinicProvider,
  mapFromAnyToClinicProvider,
  mapUnionClinicListToClinicProvidersList,
  mapVetClinicToClinicProvider,
} from '../clinic-providers.service.js'
import { dentalClinicsList } from './data/dental-clinics.js'
import { vetClinicsList } from './data/vet-clinics.js'

describe('clinicProvidersService', () => {
  describe('mapDentalClinicToClinicProvider', () => {
    it('should map dental clinic to clinical provider correctly', () => {
      const clinic: ClinicProvider = {
        name: randSuperheroName(),
        state: {
          code: '',
          name: randState(),
        },
        availability: {
          from: '10:00',
          to: '19:30',
        },
      }

      const clinicProvider = mapDentalClinicToClinicProvider({
        name: clinic.name,
        stateName: clinic.state.name as string,
        availability: clinic.availability,
      })

      expect(clinicProvider).toEqual(clinic)
    })
  })

  describe('mapVetClinicToClinicProvider', () => {
    it('should map vet clinic to clinical provider correctly', () => {
      const clinic: ClinicProvider = {
        name: randSuperheroName(),
        state: {
          code: randStateAbbr(),
          name: '',
        },
        availability: {
          from: '10:00',
          to: '19:30',
        },
      }

      const clinicProvider = mapVetClinicToClinicProvider({
        clinicName: clinic.name,
        stateCode: clinic.state.code as string,
        opening: clinic.availability,
      })

      expect(clinicProvider).toEqual(clinic)
    })
  })

  describe('mapFromAnyToClinicProvider', () => {
    it('should map from vet clinic to clinic provider correctly', () => {
      const clinic: ClinicProvider = {
        name: randSuperheroName(),
        state: {
          code: randStateAbbr(),
          name: '',
        },
        availability: {
          from: '10:00',
          to: '19:30',
        },
      }

      const vetClinic = {
        clinicName: clinic.name,
        stateCode: clinic.state.code as string,
        opening: clinic.availability,
      } as VetClinicProvider

      const clinicProvider = mapFromAnyToClinicProvider(vetClinic)
      expect(clinicProvider).toEqual(clinic)
    })

    it('should map from dental clinic to clinic provider correctly', () => {
      const clinic: ClinicProvider = {
        name: randSuperheroName(),
        state: {
          code: '',
          name: randState(),
        },
        availability: {
          from: '10:00',
          to: '19:30',
        },
      }

      const dentalClinic = {
        name: clinic.name,
        stateName: clinic.state.name as string,
        availability: clinic.availability,
      } as DentalClinicProvider

      const clinicProvider = mapFromAnyToClinicProvider(dentalClinic)
      expect(clinicProvider).toEqual(clinic)
    })
  })

  describe('mapUnionClinicListToClinicProvidersList', () => {
    it('should map vet clinic and dental clinic union list to clinical provider list correctly', () => {
      const clinicList: ClinicProvider[] = [
        {
          name: randSuperheroName(),
          state: {
            code: randStateAbbr(),
            name: '',
          },
          availability: {
            from: '10:00',
            to: '19:30',
          },
        },
        {
          name: randSuperheroName(),
          state: {
            code: randStateAbbr(),
            name: '',
          },
          availability: {
            from: '10:00',
            to: '19:30',
          },
        },
        {
          name: randSuperheroName(),
          state: {
            code: '',
            name: randState(),
          },
          availability: {
            from: '10:00',
            to: '19:30',
          },
        },
      ]

      const unionClinicList: (VetClinicProvider | DentalClinicProvider)[] = [
        {
          clinicName: clinicList[0].name,
          opening: clinicList[0].availability,
          stateCode: clinicList[0].state.code,
        } as VetClinicProvider,
        {
          clinicName: clinicList[1].name,
          opening: clinicList[1].availability,
          stateCode: clinicList[1].state.code,
        } as VetClinicProvider,
        {
          name: clinicList[2].name,
          availability: clinicList[2].availability,
          stateName: clinicList[2].state.name,
        } as DentalClinicProvider,
      ]

      const clinicProvidersList =
        mapUnionClinicListToClinicProvidersList(unionClinicList)

      expect(clinicProvidersList).toEqual(clinicList)
    })
  })

  describe('filter by multiple criteria', () => {
    const clinicProvidersList = mapUnionClinicListToClinicProvidersList([
      ...dentalClinicsList,
      ...vetClinicsList,
    ])

    describe('match all the criterias', () => {
      describe('clinic name criteria', () => {
        it('should be able to filter by half of the clinic name', () => {
          const name = 'Good'
          const criteria: Criteria = { name: 'Good' }
          const filteredClinics = filterClinicsByMultipleCriteria(
            clinicProvidersList,
            criteria,
          )
          expect(filteredClinics).length(2)
          expect(filteredClinics[0].name).toContain(name)
          expect(filteredClinics[1].name).toContain(name)
        })

        it('should be able to filter by entire clinic name', () => {
          const name = 'UAB Hospital'
          const criteria: Criteria = { name }
          const filteredClinics = filterClinicsByMultipleCriteria(
            clinicProvidersList,
            criteria,
          )
          expect(filteredClinics).length(1)
          expect(filteredClinics[0].name).toBe(name)
        })
      })

      describe('state code and state name criteria', () => {
        it('should be able to filter by state name', () => {
          const stateName = 'Arizona'
          const criteria: Criteria = { stateName }
          const filteredClinics = filterClinicsByMultipleCriteria(
            clinicProvidersList,
            criteria,
          )

          expect(filteredClinics).length(1)
          expect(filteredClinics[0].state.name).toBe(stateName)
        })

        it('should be able to filter by state code', () => {
          const stateCode = 'KS'
          const criteria: Criteria = { stateCode }
          const filteredClinics = filterClinicsByMultipleCriteria(
            clinicProvidersList,
            criteria,
          )

          expect(filteredClinics).length(1)
          expect(filteredClinics[0].state.code).toBe(stateCode)
        })
      })

      describe('availability criteria', () => {
        it('should be able to filter by availability from hour equal or greater', () => {
          const availabilityFrom = '08'
          const criteria: Criteria = { availabilityFrom }
          const filteredClinics = filterClinicsByMultipleCriteria(
            clinicProvidersList,
            criteria,
          )

          expect(filteredClinics).length(10)
        })
      })

      it('should be able to filter by clinic name and state name', () => {
        const stateName = 'alaska'
        const name = 'UAB'
        const criteria: Criteria = { name, stateName }
        const filteredClinics = filterClinicsByMultipleCriteria(
          clinicProvidersList,
          criteria,
        )

        expect(filteredClinics).length(1)
        expect(filteredClinics[0].state.name).toMatch(
          new RegExp(stateName, 'i'),
        )
        expect(filteredClinics[0].name).toContain(name)
      })

      it('should be able to filter by clinic name and availability', () => {
        const name = 'good'
        const availabilityFrom = '15'
        const availabilityTo = '20'
        const criteria: Criteria = { name, availabilityFrom, availabilityTo }
        const filteredClinics = filterClinicsByMultipleCriteria(
          clinicProvidersList,
          criteria,
        )

        expect(filteredClinics).length(1)
        expect(filteredClinics[0].name).toMatch(new RegExp(name, 'i'))
        expect(filteredClinics[0].availability.from).toContain(availabilityFrom)
        expect(filteredClinics[0].availability.to).toContain(availabilityTo)
      })

      it('should be able to filter by state and availability', () => {
        const stateCode = 'CA'
        const availabilityFrom = '00'
        const availabilityTo = '24'
        const criteria: Criteria = {
          stateCode,
          availabilityFrom,
          availabilityTo,
        }
        const filteredClinics = filterClinicsByMultipleCriteria(
          clinicProvidersList,
          criteria,
        )

        expect(filteredClinics).length(2)
        expect(filteredClinics[0].state.code).toMatch(
          new RegExp(stateCode, 'i'),
        )
      })

      it('should be able to filter by clinic name and state and availability', () => {
        const name = 'National'
        const stateCode = 'CA'
        const availabilityFrom = '00'
        const availabilityTo = '22:30'
        const criteria: Criteria = {
          name,
          stateCode,
          availabilityFrom,
          availabilityTo,
        }
        const filteredClinics = filterClinicsByMultipleCriteria(
          clinicProvidersList,
          criteria,
        )

        expect(filteredClinics).length(1)
        expect(filteredClinics[0].name).toMatch(new RegExp(name, 'i'))
        expect(filteredClinics[0].state.code).toMatch(
          new RegExp(stateCode, 'i'),
        )
        expect(filteredClinics[0].availability.from).toContain(availabilityFrom)
        expect(filteredClinics[0].availability.to).toContain(availabilityTo)
      })
    })
  })
})
