import { ClinicProvider } from '../../schemas/clinic-provider.js'
import {
  dentalClinicProvider,
  DentalClinicProvider,
} from '../../schemas/dental-clinic-provider.js'
import {
  vetClinicProvider,
  VetClinicProvider,
} from '../../schemas/vet-clinic-provider.js'
import { compareHours } from '../../utils/utils.js'
import { validateDataBasedOnSchema } from '../../utils/validate-schema.js'
import { ClinicProvidersListQueryString } from './clinic-providers.schema.js'

export type Criteria = ClinicProvidersListQueryString

export const filterClinicsByMultipleCriteria = (
  clinics: ClinicProvider[],
  criteria: Criteria,
): ClinicProvider[] => {
  return clinics.filter((item) => {
    const { name, stateCode, stateName, availabilityFrom, availabilityTo } =
      criteria

    const isNameMatch =
      !name || item.name.toLowerCase().includes(name.toLowerCase())
    const isStateCodeMatch =
      !stateCode || item.state.code.toLowerCase() === stateCode.toLowerCase()
    const isStateNameMatch =
      !stateName || item.state.name.toLowerCase() === stateName.toLowerCase()
    const isAvailabilityFromMatch =
      !availabilityFrom ||
      compareHours(
        item.availability.from,
        'greaterThenOrEqual',
        availabilityFrom,
      )
    const isAvailabilityToMatch =
      !availabilityTo ||
      compareHours(item.availability.to, 'lessThenOrEqual', availabilityTo)

    return (
      isNameMatch &&
      isStateCodeMatch &&
      isStateNameMatch &&
      isAvailabilityFromMatch &&
      isAvailabilityToMatch
    )
  })
}

export const mapUnionClinicListToClinicProvidersList = (
  clinicsList: (DentalClinicProvider | VetClinicProvider)[],
): ClinicProvider[] =>
  clinicsList
    .map((clinic) => mapFromAnyToClinicProvider(clinic))
    .filter(Boolean) as ClinicProvider[]

export const mapFromAnyToClinicProvider = (
  clinic: DentalClinicProvider | VetClinicProvider,
): ClinicProvider | undefined => {
  if (validateDataBasedOnSchema(dentalClinicProvider, clinic)) {
    return mapDentalClinicToClinicProvider(clinic as DentalClinicProvider)
  }

  if (validateDataBasedOnSchema(vetClinicProvider, clinic)) {
    return mapVetClinicToClinicProvider(clinic as VetClinicProvider)
  }

  return undefined
}

export const mapVetClinicToClinicProvider = (
  clinic: VetClinicProvider,
): ClinicProvider => ({
  name: clinic.clinicName,
  state: {
    name: '',
    code: clinic.stateCode,
  },
  availability: clinic.opening,
})

export const mapDentalClinicToClinicProvider = (
  clinic: DentalClinicProvider,
): ClinicProvider => ({
  name: clinic.name,
  state: {
    name: clinic.stateName,
    code: '',
  },
  availability: clinic.availability,
})
