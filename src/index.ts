import dotenv from 'dotenv'
dotenv.config()
import * as Server from './server.js'

Server.start()
