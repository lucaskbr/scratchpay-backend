import got from 'got'
import { DentalClinicProvider } from '../schemas/dental-clinic-provider.js'
import { VetClinicProvider } from '../schemas/vet-clinic-provider.js'

export const listVetClinics = async (): Promise<VetClinicProvider[]> => {
  const response = await got(
    'https://storage.googleapis.com/scratchpay-code-challenge/vet-clinics.json',
  ).json<VetClinicProvider[]>()

  return response
}

export const listDentalClinics = async (): Promise<DentalClinicProvider[]> => {
  return got(
    'https://storage.googleapis.com/scratchpay-code-challenge/dental-clinics.json',
  ).json<DentalClinicProvider[]>()
}
