import { Static, Type } from '@sinclair/typebox'

export const opening = Type.Object({
  from: Type.String(),
  to: Type.String(),
})

export const vetClinicProvider = Type.Object({
  clinicName: Type.String(),
  stateCode: Type.String(),
  opening: opening,
})

export type Opening = Static<typeof opening>

export type VetClinicProvider = Static<typeof vetClinicProvider>
