import { Static, Type } from '@sinclair/typebox'

export const availability = Type.Object({
  from: Type.String(),
  to: Type.String(),
})

export type Availability = Static<typeof availability>

export const dentalClinicProvider = Type.Object({
  name: Type.String(),
  stateName: Type.String(),
  availability: availability,
})

export type DentalClinicProvider = Static<typeof dentalClinicProvider>
