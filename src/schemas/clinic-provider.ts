import { Static, Type } from '@sinclair/typebox'

export const availability = Type.Object({
  from: Type.String(),
  to: Type.String(),
})

export const clinicProvider = Type.Object({
  name: Type.String(),
  state: Type.Object({
    name: Type.String(),
    code: Type.String(),
  }),
  availability: availability,
})

export type ClinicProvider = Static<typeof clinicProvider>
