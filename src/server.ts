import Fastify from 'fastify'
import { bootstrap } from 'fastify-decorators'
import path from 'path'
import { fileURLToPath } from 'url'
import fastifySwagger from '@fastify/swagger'
import fastifySwaggerUi from '@fastify/swagger-ui'
import cors from '@fastify/cors'

const __filename = fileURLToPath(import.meta.url)

const __dirname = path.dirname(__filename)

const fastify = Fastify({
  logger: true,
})

await fastify.register(cors, {})

// Swagger needs to be defined before any route
fastify.register(fastifySwagger, {
  swagger: {
    info: {
      title: 'Scratchpay swagger',
      description: 'Scratchpay API',
      version: '0.1.0',
    },
    schemes: ['https'],
    host: 'localhost',
    basePath: process.env.BASE_PATH,
    consumes: ['application/json'],
    produces: ['application/json'],
  },
})

fastify.register(fastifySwaggerUi, {
  routePrefix: '/docs',
  uiConfig: {
    docExpansion: 'none',
    deepLinking: false,
  },
})

// Register handlers auto-bootstrap
fastify.register(bootstrap, {
  // Specify directory with our handler
  directory: path.resolve(__dirname, `modules`),

  // Specify mask to match only our handler
  mask: new RegExp('.*.controller.(js|ts)$'),
})

export const start = async (): Promise<void> => {
  try {
    await fastify.ready()
    await fastify.listen({
      port: Number(process.env.PORT) || 8080,
      host: '0.0.0.0',
    })
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
